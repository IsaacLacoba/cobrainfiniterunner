#!/usr/bin/python
# -*- coding: utf-8; mode: python -*-
from math import pow, sin, asin, pi
import random
from vector2 import Vector2 as vec2


def elasticOut(self, k):
    a,p = 0.1,0.4
    if k==0: return 0
    if k==1: return 1
    if not a or a < 1:a=1;s=p/4
    else: s=p*asin(1/a)/(2*pi)
    return (a* pow(2, -10*k) *
            sin((k-s) * (2 * pi) / p) + 1)


def elasticIn(k):
    a,p = 0.1,0.4
    if k==0: return 0
    if k==1: return 1
    if not a or a < 1:a=1;s=p/4
    else: s=p * asin(1/a)/(2 * pi)
    
    return - (a * pow(2, 10 * (k - 1)) *
              sin((k - s) * (2 *pi) / p))
    

def elasticInOut(k):
    a,p = 0.1,0.4
    if k==0: return 0
    if k==1: return 1
    if not a or a < 1:a=1;s=p/4
    else: s=p * asin(1/a)/(2 * pi)
    
    if k*2<1:
        return (-0.5 * (a * pow(2,10*(k-1)) *
                        sin((k-s)*(2*pi)/p)))
    
    return a * pow(2, -10* (k-1)) * sin((k-s) * (2 * pi) /p ) * 0.5 + 1


def shake(k):
    try:
        if k==0:return vec2(-10,-10)
        if k==1:return vec2(random.randint(-20,0), random.randint(-20,0))
        else:return vec2(random.randint(-10-int(10*k),0-int(10*k)), random.randint(-10-int(10*k),0-int(10*k)))
    except ValueError:
        print k
        
