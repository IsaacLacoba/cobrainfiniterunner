import pygame
from pygame.locals import *

import random
from vector2 import Vector2 as vec2

from player import player
from floor import floor
from building import building
from physics import *

class Game:
    def __init__(self):
        pygame.init()
        pygame.font.init()
        self.load_configuration()
        self.create_scene()
        
    def load_configuration(self):
        self.fontSize = 25
        self.font1 = pygame.font.Font(None, self.fontSize)
        
        self.screen_size = 800,600
        self.width,self.height = self.screen_size
        
        self.screen = pygame.display.set_mode(self.screen_size)
        self.clock = pygame.time.Clock()
        
        self.offset = 20
        self.whole_width,self.whole_height =  self.width + self.offset, self.height + self.offset
        self.whole_surface = pygame.Surface((self.whole_width, self.whole_height))
        
        self.draw_pos = vec2(-10,-10)
        
        self.player1 = player((self.whole_width/2, self.whole_height-46))
        self.lose = True
        self.reset()
        
        self.gravity = 550
               
        self.shakeTimer = 0.1;
        self.maxShakeTimer = self.shakeTimer;
        
        self.slamming = False
        
        self.building_list = []
        self.to_delete = []
        self.count = 0

        self.key_binder = {
            K_ESCAPE: self.exit, 
            K_SPACE: self.player1.jump,
            K_r: self.reset
        }
        
    def create_scene(self):
        for i in range(150):
            self.building_list.append(building(random.randint(0, self.whole_width),(random.randint(1,7)),
                                          self.whole_height))
    def exit(self):
        self.done = True

    def reset(self):
        if self.lose == False:
            return
        
        self.time=0
        self.distance=0
        self.player1.pos.y = self.whole_height-46
        self.player1.vel.x = 600
        self.floor_group = pygame.sprite.Group(floor((self.whole_width/2, self.whole_height-30)))
        self.lose = False        
        
    def loop(self):
        self.done = False
        while not self.done:
            time_passed = self.clock.tick(60)
            time_passed_seconds = time_passed/1000.0
    
            if not self.lose:
                self.time += time_passed_seconds
                
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.done = True
            
            pressed_keys = pygame.key.get_pressed()
            for key in self.key_binder:
                if pressed_keys[key]:    
                    self.key_binder[key]()
            
            for i in range(self.count):
                self.building_list.append(building(self.whole_width,(random.randint(1,7)), self.whole_height))
                self.count = 0
        
            self.building_list = sorted(self.building_list, key=lambda building:
                                        building.z+(building.pos.x/float(self.width*2)))
            self.building_list.reverse()
    
            #new_pointlist = sorted(point_list, key=lambda point: point.get_distance(currentPoint))
            self.sorted_sprites = sorted(self.floor_group.sprites(), key=lambda floor: floor.pos.x)
            if self.sorted_sprites[-1].width + self.sorted_sprites[-1].pos.x < self.whole_width:
                self.floor_group.add(floor((self.sorted_sprites[-1].pos.x +
                                       self.sorted_sprites[-1].width +
                                       random.randint(500,750), self.whole_height-30)))
        
                                        
            self.player1.update(time_passed_seconds, 0, self.floor_group, self.gravity)
            if self.player1.pos.y > self.whole_height+16:
                self.lose = 1
                self.player1.vel.x,self.player1.vel.y = 0,0
        
            self.floor_group.update(self.player1.vel.x, time_passed_seconds)
            for FLOOR in self.floor_group.sprites():
                if FLOOR.width + FLOOR.pos.x < 0:
                    self.floor_group.remove(FLOOR)
            
            self.distance+=int(self.player1.vel.x * time_passed_seconds)
    
            if self.player1.slammed or self.slamming:
                self.slamming = True
                self.draw_pos = shake(elasticIn(self.shakeTimer / self.maxShakeTimer))
                self.shakeTimer -= time_passed_seconds
                if self.shakeTimer <= 0:
                    self.shakeTimer = self.maxShakeTimer
                    self.draw_pos = (-10,-10)
                    self.player1.slammed=False
                    self.slamming = False
            
            self.screen.fill((255,0,0))
    
            self.whole_surface.fill((82,139,139))
            pygame.draw.rect(self.whole_surface, (120,120,120),
                             (0,self.whole_height-7*31,self.whole_width,self.whole_height/2))
            pygame.draw.rect(self.whole_surface, (0,0,0),
                             (0,self.whole_height-7*31,self.whole_width,self.whole_height/2), 1)
            for BUILDING in self.building_list:
                BUILDING.update(self.player1.vel.x, time_passed_seconds)
                if BUILDING.pos.x + BUILDING.width < 0:
                    self.to_delete.append(BUILDING)
                    continue
                BUILDING.render(self.whole_surface)
        
            for BUILDING in self.to_delete:
                self.building_list.remove(BUILDING)
                self.count+=1
        
            del self.to_delete[:]
    
            self.floor_group.draw(self.whole_surface)
            self.player1.render(self.whole_surface)
    
            time_render = self.font1.render(str(self.time), True, (0,0,0))
            distance_render = self.font1.render(str(self.distance), True, (0,0,0))
    
            self.whole_surface.blit(time_render, (20,20))
            self.whole_surface.blit(distance_render, (self.whole_width-distance_render.get_size()[0]-20, 20))
    
            if self.lose:
                lose_render = self.font1.render("GAME OVER. press R to retry", True, (0,0,0))
                lose_box = lose_render.get_rect()
                lose_box.center = (self.whole_width/2, self.whole_height/3)
                self.whole_surface.blit(lose_render, lose_box)
        
            self.screen.blit(self.whole_surface, self.draw_pos)
    
            pygame.display.update()
            pygame.display.set_caption(str(self.clock.get_fps())+ ", " + str(self.count))
    
        pygame.quit()

game = Game()            
game.loop()
